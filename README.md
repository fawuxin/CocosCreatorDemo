# CocosCreatorDemo

**CocosCreatorDemo is a set of demos commonly used in cocoscreator game development.**

**CocosCreatorDemo是一个CocosCreator游戏开发常用的功能实现的demo集合。**

*Cocos Creator版本：2.4.3*

- 打字机---TypeWriter
  用途：文字对话，逐字出现文字。常用于新手引导文字对话、人物对话

- 跑马灯---HorseRaceLamp
  用途：在一个区域内，实现文字滚动。常用于游戏公告、弹幕消息

- 背景无限滚动---BgRollInfinite  
  用途：让背景循环滚动起来。

- 遥控杆---Rocker
  用途：通过遥杆控制目标移动方向。

- 滚动式组件---ScrollItemList
  用途：让多个子item在一个区域内实现滚动。常用于小游戏内，作为跳转其他小游戏的入口，做滚动效果。
  
- JsZip解压缩zip文件---JsZip

  用途：利用JsZip库，实现对zip文件的加载并解析。
  
- ScrollView分帧加载Item---FrameLoading

  用途：
  
  1.ScrollView 支持分帧加载item，优化卡顿
  
  2.优化Drawcall，不在科室范围内，Drawcall不计算
  
  3.可以滚动ScrollView，使指定index的item在ScrollView试图中间
  
- FlipCard---翻牌效果

- RoundAvatar---圆形头像

- ScollingNum---滚动数字

- FrameAnimation---帧动画组件

  用途：输入SpriteFrame数组，播放时间（秒为单位），是否循环播放。即可实现帧动画。不用一帧一帧编辑帧动画。
  
- StreamerShader---流光Shader

  用途：流光效果，在游戏中，比如某个卡牌的稀有度，可以用流光shader来突出。
  
- ScreenAdapt---刘海水滴屏适配方案

- ArcProgressBar---环形进度条实现方案（之一）



