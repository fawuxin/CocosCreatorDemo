

const { ccclass, property } = cc._decorator;

@ccclass
export default class LabelScrolllingNum extends cc.Component {

    private _num: number = 0;

    private _toNum: number = 0;

    private lbNum: cc.Label = null;

    public init(num: number, durationMs: number = 50) {

        this.unschedule(this.updateNum);

        this.lbNum = this.node.getComponent(cc.Label);

        if (!this.lbNum) {
            this.lbNum = this.node.addComponent(cc.Label);
        }

        this._toNum = num;

        let seconds = durationMs / 1000;
        this.schedule(this.updateNum, seconds);
    }

    public scrollToNum(curNum: number, nextNum: number, totalTime: number) {

        this.unschedule(this.updateNum);

        this.lbNum = this.node.getComponent(cc.Label);

        let countTimes = nextNum - curNum;

        let seconds = totalTime / countTimes

        if (!this.lbNum) {
            this.lbNum = this.node.addComponent(cc.Label);
        }

        this._toNum = nextNum;

        this.schedule(this.updateNum, seconds);
    }

    private updateNum() {
        cc.log("updateNum");

        if (this._num >= this._toNum) {
            return;
        }

        this._num++;
        this.lbNum.string = this._num + "";

        if (this._num >= this._toNum) {
            this.unschedule(this.updateNum);
        }
    }

    onDestroy() {
        this.unschedule(this.updateNum);
    }



    // update (dt) {}
}
