// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.ProgressBar)
    proBar: cc.ProgressBar = null;

    _progress: number = 0;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    update(dt) {
        if (this._progress < 1) {
            this._progress += 0.002;

            if (this._progress > 1) {
                this._progress = 1;
            }
            this.proBar.progress = this._progress;
            if (this._progress >= 1) {
                this._progress = 0;
            }
        }
    }
}
