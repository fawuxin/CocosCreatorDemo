// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class LifeCallBack extends cc.Component {

    _time: number = 0;
    _time1: number = 0;

    onLoad() {
        cc.log("onLoad")
    }

    start() {
        cc.log("start")
    }

    onEnable() {
        cc.log("onEnable")
    }

    onDisable() {
        cc.log("onDisable")
    }

    update(dt) {

        this._time += dt;

        if (this._time >= 2) {
            cc.log("update");
            this._time = 0;
        }

    }

    lateUpdate(dt) {

        this._time1 += dt;
        if (this._time1 >= 2) {
            cc.log("lateUpdate");
            this._time1 = 0;
        }
    }

    onDestroy() {
        cc.log("onDestroy");
    }

    // update (dt) {}
}
